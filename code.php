<?php

// 1
function divisiblesOfFive() {
    $count = 0;
    for ($i = 0; $i <= 1000; $i++) {
        if ($i % 5 === 0) {
            echo $i . ', ';
            $count++;
        }
        if ($count === 24) {
            echo '<br/>';
            $count = 0;
        }
    }
}

// 2

$students = [];

function addStudent($name) {
    global $students;
    array_push($students, $name);
}

function printStudents() {
    global $students;
    foreach ($students as $student) {
        echo $student . '<br/>';
    }
}

function countStudents() {
    global $students;
    return count($students);
}

function removeStudent() {
    global $students;
    array_shift($students);
}
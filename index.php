<?php require_once "./code.php"; ?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Activity</title>
</head>
<body>
    <h3>Divisibles of Five</h3>

    <div style="width: 500px; height: 400px; border: 1px solid black;">
        <?= divisiblesOfFive(); ?>
    </div>

    <h3>Add student and display Array </h3>
    <div style="width: 500px; height: 400px; border: 1px solid black;">
        <?php addStudent('John'); ?>
        <?php addStudent('Jane'); ?>
        <?php addStudent('Jill'); ?>
        <?php addStudent('Jack'); ?>
        <?php addStudent('Jenny'); ?>
        <?php addStudent('Jesse'); ?>
        <?php addStudent('Jasper'); ?>
        <?php addStudent('Jasmine'); ?>
        <?php addStudent('Jared'); ?>
    
        <?= printStudents(); ?>
    </div>

    <h3>Count Students</h3>
    <div style="width: 500px; height: 200px; border: 1px solid black;">
        Student Count: <?= countStudents(); ?>
    </div>

    <h3>Remove Student</h3>
    <div style="width: 500px; height: 300px; border: 1px solid black;">
        <?php removeStudent(); ?>
        <?= printStudents(); ?>
    </div>

</body>
</html>